﻿using ArcaneLibs;
using BotCore.DataModel;
using InstallScriptGenerator;

if (Environment.GetEnvironmentVariable("USER") != "root")
{
    Console.WriteLine("You aren't running as root!");
    Environment.Exit(1);
}

var db = Db.GetPostgres();
while (!File.Exists("DiscordBots.sln"))
{
    Environment.CurrentDirectory += "/..";
    Console.WriteLine($"Scanning for root dir... {Environment.CurrentDirectory}");
}

if (!File.Exists("DiscordBots.sln"))
{
    Console.WriteLine("You must run this script from the solution directory!");
}

Console.WriteLine("Stopping and deleting old services...");
foreach (var svc in Directory.GetFiles("/etc/systemd/system").Select(x => new FileInfo(x).Name)
             .Where(x => x.StartsWith("botcore")))
{
    Util.RunCommandSync($"systemctl", $"disable --now {svc}", true);
    File.Delete("/etc/systemd/system/" + svc);
}

Util.RunCommandSync("systemctl", "daemon-reload", true);

Console.WriteLine("Creating core services...");
StaticServiceFiles.MakeStaticServices(args);

Console.WriteLine("Creating bot services...");
var after = "botcore.web";
foreach (var bot in db.Bots.Where(x => x.Enabled && !x.RemotelyDeployed && x.Token.Length > 2))
{
    Console.WriteLine($"Writing service file: botcore.bot.{bot.Name}...");
    var content = $@"[Unit]
Description=Systemd service for {bot.FriendlyName} through BotCore.Runner

[Service]
User=root
WorkingDirectory={Environment.CurrentDirectory}/BotCore.Runner
ExecStart=/bin/dotnet run --nobuild -- {bot.Name}
Restart=always
Type=notify
NotifyAccess=all

[Install]
WantedBy=multi-user.target
After={after}";
    File.WriteAllText($"/etc/systemd/system/botcore.bot.{bot.Name}.service", content);
    after = "botcore.bot." + bot.Name;
}

Console.WriteLine("Building bots...");
Util.RunCommandSync("dotnet", "build", true);
Console.WriteLine("Reloading systemd");
Util.RunCommandSync("systemctl", "daemon-reload", true);
Console.WriteLine("Starting services...");
foreach (var svc in Directory.GetFiles("/etc/systemd/system").Select(x => new FileInfo(x).Name)
             .Where(x => x.StartsWith("botcore.bot.")))
{
    if (args.Contains("--enable")) Util.RunCommandSync($"systemctl", $"enable {svc}", true);
    if (args.Contains("--start")) Util.RunCommandSync($"systemctl", $"start {svc}", true);
}


Console.WriteLine(@"NGINX config for the web service.
You can add multiple server_name lines if needed, the web service automatically detects which bot you're using:

server {
    server_name botname.domain.tld;
    listen 80;
    listen 443 ssl http2;
    #your ssl/other setup here here
    location / {
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection ""upgrade"";
        proxy_pass_request_headers on;
        proxy_hide_header Access-Control-Allow-Origin;
        add_header Access-Control-Allow-Origin *;
        proxy_ssl_server_name on;
        proxy_cache_key $scheme://$host$request_uri;
        proxy_pass http://localhost:5033;
    }
}");

Console.WriteLine("Services:");
Console.WriteLine(string.Join("\n",
    Directory.GetFiles("/etc/systemd/system")
        .Select(x => new FileInfo(x).Name.Replace(".service", "")).Where(x => x.StartsWith("botcore."))));

