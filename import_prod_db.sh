#!/bin/sh
DB='discordbots'
echo "Dropping table \`$DB\`..."
echo 'DROP DATABASE discordbots WITH (force);' | psql -U postgres
echo Connecting...
ssh thearcanebrony.net 'sudo su postgres -c "pg_dump --no-comments -C discordbots"' | psql -U postgres -e
echo 'update bot set "Enabled" = true' | psql -U postgres -e -d $DB
echo 'update server set "Prefix" = concat('\''d'\'',"Prefix") where true' | psql -U postgres -e -d $DB
