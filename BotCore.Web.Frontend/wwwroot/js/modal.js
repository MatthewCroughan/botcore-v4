function PrepareModal() {
    console.log("preparing modal")
    $("body").append(`<div id='tabmodal-bg'></div>`);
    $("#tabmodal-bg").click(CloseModal);
}

function CloseModal(a) {
    if (a.target.id === "tabmodal-bg") {
        HideOverflowRecursive(a.target);
        $(".tabmodal").animate({
            opacity: 0,
            width: 0,
            height: 0
        }, 500, function () {
            $(".tabmodal").remove();
        });
        $("#tabmodal-bg").animate({
            opacity: 0
        }, 500, function () {
            $("#tabmodal-bg").remove();
        });
    }
}

function OpenModal(url) {
    console.log(`opening modal (${url})`)
    PrepareModal();
    $("#tabmodal-bg").append(`<div id='userpanel' class='tabmodal'><iframe src='${url}' style="width: 100%; height: 100%; border-radius: 25px;"/></div>`);
}

function HideOverflowRecursive(elem){
    console.log('Hiding overflow for: ' + elem)
    elem.style.overflow = 'hidden !important';
    for (let ch of elem.children) {
        HideOverflowRecursive(ch);
    }
}