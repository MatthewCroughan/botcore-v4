﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace BotCore.DataModel.Migrations
{
    public partial class add_anti_scam_log : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "anti_scam_log",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ServerId = table.Column<long>(type: "bigint", nullable: false),
                    UserId = table.Column<long>(type: "bigint", nullable: false),
                    MessageText = table.Column<string>(type: "text", nullable: false),
                    Time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_anti_scam_log", x => x.Id);
                    table.ForeignKey(
                        name: "FK_anti_scam_log_d_server_user_UserId",
                        column: x => x.UserId,
                        principalTable: "d_server_user",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_anti_scam_log_server_ServerId",
                        column: x => x.ServerId,
                        principalTable: "server",
                        principalColumn: "ServerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_anti_scam_log_ServerId",
                table: "anti_scam_log",
                column: "ServerId");

            migrationBuilder.CreateIndex(
                name: "IX_anti_scam_log_UserId",
                table: "anti_scam_log",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "anti_scam_log");
        }
    }
}
