﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace BotCore.DataModel.Migrations
{
    public partial class add_slowmode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "slowmode_config",
                columns: table => new
                {
                    SlowmodeId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DiscordChannelId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    ServerId = table.Column<long>(type: "bigint", nullable: false),
                    SlowmodeTime = table.Column<TimeSpan>(type: "interval", nullable: false),
                    MessageEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    ModsBypass = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_slowmode_config", x => x.SlowmodeId);
                    table.ForeignKey(
                        name: "FK_slowmode_config_server_ServerId",
                        column: x => x.ServerId,
                        principalTable: "server",
                        principalColumn: "ServerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "slowmode_data",
                columns: table => new
                {
                    SlowmodeDataId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SlowmodeConfigSlowmodeId = table.Column<long>(type: "bigint", nullable: false),
                    ServerId = table.Column<long>(type: "bigint", nullable: false),
                    UserId = table.Column<long>(type: "bigint", nullable: false),
                    BlockedSince = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_slowmode_data", x => x.SlowmodeDataId);
                    table.ForeignKey(
                        name: "FK_slowmode_data_d_server_user_UserId",
                        column: x => x.UserId,
                        principalTable: "d_server_user",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_slowmode_data_server_ServerId",
                        column: x => x.ServerId,
                        principalTable: "server",
                        principalColumn: "ServerId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_slowmode_data_slowmode_config_SlowmodeConfigSlowmodeId",
                        column: x => x.SlowmodeConfigSlowmodeId,
                        principalTable: "slowmode_config",
                        principalColumn: "SlowmodeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_slowmode_config_ServerId",
                table: "slowmode_config",
                column: "ServerId");

            migrationBuilder.CreateIndex(
                name: "IX_slowmode_data_ServerId",
                table: "slowmode_data",
                column: "ServerId");

            migrationBuilder.CreateIndex(
                name: "IX_slowmode_data_SlowmodeConfigSlowmodeId",
                table: "slowmode_data",
                column: "SlowmodeConfigSlowmodeId");

            migrationBuilder.CreateIndex(
                name: "IX_slowmode_data_UserId",
                table: "slowmode_data",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "slowmode_data");

            migrationBuilder.DropTable(
                name: "slowmode_config");
        }
    }
}
