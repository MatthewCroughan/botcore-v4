#!/bin/sh
delay=10

title='BotCore monitoring'
PROMPT_COMMAND='echo -en "\033]0;"'$title'"\a"'
printf "\e]2;$title\a"
echo 'Settings:'
echo "Htop restart delay (delay) = $delay"

echo 'Ctrl+b d to exit'
sleep 3
tmux kill-session -t ses-0
tmux new-session -s ses-0 -n monitor -d "while true; do (sleep $delay; killall htop) & htop -p $(pgrep -f BotCore -d ,); done"
for i in $(ls -1 /etc/systemd/system/botcore.* | xargs -i% basename %)
do
    tmux split-window -h
    tmux send-keys "journalctl -xefu $i | lnav -c ':goto -1'" C-m
done
tmux select-layout even-vertical
tmux attach-session -t ses-0