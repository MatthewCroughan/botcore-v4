﻿using BotCore.DbExtras;

namespace BotCore.Commands.Commands.Groups;

internal class EconomyCommands : CommandGroup
{
    private static readonly Random rnd = new();

    //daily command, gives user a random amount of credits, can only be ran once every 24 hours
    public static Command DailyCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "daily",
            Category = "Economy",
            Description = "Recieve your daily credits",
            action = async (me, ce) =>
            {
                //check if user has ran the command within 24h 
                if (DateTime.Now.Subtract(me.User.LastCreditsRedeem).TotalDays >= 1)
                {
                    //get random credits count
                    var addCreds = rnd.Next(1, 20);
                    //50% more if premium
                    if (me.User.IsPremium) addCreds = (int) (addCreds * 1.5);
                    //add credits and update last used time
                    me.User.Credits += addCreds;
                    me.User.LastCreditsRedeem = DateTime.Now;
                    await me.Channel.SendMessageAsync(
                        $"You received {addCreds} credits!{(me.User.IsPremium ? "" : $" You can get 50% more credits daily by going premium! See {me.Server.Prefix}premium for information!")}");
                }
                else
                {
                    //user has used within 24h, show how long to wait
                    var waitTime = DateTime.Now.Subtract(me.User.LastCreditsRedeem);
                    await me.Channel.SendMessageAsync(
                        $"You need to wait {23 - waitTime.Hours} hours, {59 - waitTime.Minutes} minutes and {59 - waitTime.Seconds} seconds");
                }

                return null;
            }
        };
    }

    //slots command, essentially a slot machine
    public static Command SlotsCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "slots",
            Category = "Economy",
            Description = "Let's see if you win",
            action = async (me, ce) =>
            {
                //check if user input valid amount and has that many
                if (ce.Args.Count == 1 && int.TryParse(ce.Args[0], out var inCredits) && me.User.Credits >= inCredits && inCredits > 0)
                {
                    //remove from balance
                    me.User.Credits -= inCredits;
                    //possible emotes
                    string[] possibleSlots = {":unicorn:", ":gun:", ":horse:", ":cherries:", ":cherry_blossom:"};
                    var slots = new List<string>();
                    double multiplier = 0;
                    //random slots
                    for (var i = 0; i < 3; i++)
                    {
                        slots.Add(possibleSlots[rnd.Next(0, possibleSlots.Length)]);
                    }

                    //calculate multiplier
                    if (slots[0] == slots[1] || slots[1] == slots[2] || slots[0] == slots[2]) multiplier = 1.25;
                    if (slots[0] == slots[1] && slots[1] == slots[2])
                    {
                        multiplier = 2;
                        if (slots[0] == ":unicorn:") inCredits = 50;
                    }

                    //calculate credits and send state
                    var outCredits = (int) (inCredits * multiplier);
                    me.User.Credits += outCredits;
                    await me.Channel.SendMessageAsync(
                        $"--------------------\n|{string.Join("|", slots)}|\n--------------------\nYou have {(outCredits <= 0 ? $"lost {inCredits}" : $"won {outCredits}")} credits and now have {me.User.Credits} credits!");
                }
                else
                {
                    await me.Channel.SendMessageAsync("You entered an amount of credits that you don't have!");
                }

                return null;
            }
        };
    }

    //bal command, displays a user's current balance
    public static Command BalCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "bal",
            Category = "Economy",
            Description = "Display your current balance",
            action = async (me, ce) =>
            {
                await me.Channel.SendMessageAsync(
                    $"**{me.MentionedDSUser.GlobalUser.Username}** has {me.MentionedDSUser.GlobalUser.Credits} credits.");
                return null;
            }
        };
    }
}