using System.Diagnostics;
using BotCore.DbExtras;
using BotCore.Util;
using DSharpPlus;
using DSharpPlus.Entities;

namespace BotCore.Commands.Commands.Groups;

public class CoreCommands : CommandGroup
{
    //help command
    public static Command HelpCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "help",
            Category = "Core",
            Description = "Help command",
            action = async (me, ce) =>
            {
                //get command list for bot
                bot.CommandManager.Commands = bot.CommandManager.Commands.OrderBy(x => x.Category).ToList();
                var helpText = $"**{bot.Bot.FriendlyName}** help\nPrefix: {me.Server.Prefix}\n\n";
                foreach (var d in bot.CommandManager.Commands)
                {
                    //add commands if user has permission to execute it and command is enabled in guild
                    if (await d.HasPermission(me, ce) &&
                        !me.Server.DisabledCommands.Contains(d.Name))
                        helpText += $"[{d.Category}] `{d.Name}`: {d.Description}{(d.Donator ? " (Donator)" : "")}\n";

                    //Send message if its getting too long for a single message
                    if (helpText.Length >= 1900)
                    {
                        await me.Channel.SendMessageAsync(helpText);
                        helpText = "";
                    }
                }

                helpText +=
                    $"*This bot was written by **The Arcane Brony** and friends.\nSee {me.Server.Prefix}devs for more!*";
                return await me.Channel.SendMessageAsync(helpText);
            }
        };
    }

    //Developer list
    public static Command DevsCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "devs",
            Category = "Core",
            Description = "List of developers",
            action = async (me, ce) =>
            {
                //Get commit counts by username
                var commitCounts = ArcaneLibs.Util.GetGitCommitCounts();
                var cc = "";
                //stitch together a list
                foreach (var commitCount in commitCounts)
                {
                    cc += $" - {commitCount.Key} ({commitCount.Value} commits)\n";
                }

                //send
                await me.Channel.SendMessageAsync("Our wonderful developers:\n" +
                                                  cc + "\n\n" +
                                                  $"Total commit count: {commitCounts.Sum(x => x.Value)}");
                return null;
            }
        };
    }

    //User/bot count in current guild
    public static Command UserCountCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "usercount",
            Category = "Core",
            Description = "Amount of users and bots",
            action = async (me, ce) =>
            {
                int users = 0, bots = 0, online = 0;
                IReadOnlyCollection<DiscordMember> members = null;
                try
                {
                    //get all members in guild, if possible
                    members = await me.Channel.Guild.GetAllMembersAsync();
                }
                catch
                {
                    return await me.Channel.SendMessageAsync(
                        "Members intent not enabled on this bot! Please report this!");
                }

                if (members != null)
                    foreach (var user in members)
                        if (user.IsBot) //Check if bot
                        {
                            bots++;
                        }
                        else
                        {
                            users++;
                            //check if not offline/invisible
                            if (user.Presence != null && !(user.Presence.Status == UserStatus.Offline ||
                                                           user.Presence.Status == UserStatus.Invisible))
                                online++;
                        }

                await me.Channel.SendMessageAsync(
                    "```moonscript\n" +
                    "Member count\n" +
                    "------------\n" +
                    $" Users: {users}\n" +
                    $"Online: {online}\n" +
                    $"  Bots: {bots}\n" +
                    "+-----------\n" +
                    $" Total: {users + bots}```");
                return null;
            }
        };
    }

    //Cleanup bot messages, and the commands that triggered them if permissions are granted
    public static Command CleanupCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "cleanup",
            Category = "Core",
            Description = $"Clean up {bot.Bot.Name} spam",
            action = async (me, ce) =>
            {
                var c = 0;
                //fetch messages that are sent by the bot or start with prefix
                List<DiscordMessage> msgs = (await me.Channel.GetMessagesAsync(500)).ToList().FindAll(message =>
                    message.Author.Id == me.Bot.DiscordClient.CurrentUser.Id ||
                    message.Content.StartsWith(me.Server.Prefix));
                //check if bot has permissions to delete messages from other users
                var canDeleteCommands = me.Channel.Guild.Members.TryGetValue(me.Bot.DiscordClient.CurrentUser.Id,
                                            out var member) &&
                                        me.Channel.PermissionsFor(member).HasPermission(Permissions.ManageMessages);
                for (var i = 0; i < msgs.Count; i++)
                {
                    if (canDeleteCommands || msgs[i].Author.Id == me.Bot.DiscordClient.CurrentUser.Id)
                        try
                        {
                            await msgs[i].DeleteAsync();
                            c++;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("An exception occurred deleting a message:\n" + e);
                        }

                    //Get more messages if we reached the end of the list
                    if (i == msgs.Count - 1)
                    {
                        msgs = (await me.Channel.GetMessagesBeforeAsync(msgs[i].Id, 500)).ToList().FindAll(message =>
                        {
                            return message.Author.Id == me.Bot.DiscordClient.CurrentUser.Id ||
                                   message.Content.StartsWith(me.Server.Prefix);
                        });
                        i = 0;
                    }
                }

                var msg = await me.Channel.SendMessageAsync($"Deleted {c} messages!");
                new Thread(() =>
                {
                    //Delete result message after 5 seconds
                    Thread.Sleep(5000);
                    msg.DeleteAsync();
                }).Start();
                return null;
            }
        };
    }

    //userinfo command: displays info about a user
    //TODO: implement pinged member handling
    public static Command UserInfoCommand(BotImplementation bot)
    {
        return new Command()
        {
            Name = "userinfo",
            Category = "Core",
            Description = "Info about a user",
            action = async (me, ce) =>
            {
                List<DiscordMember> members = new List<DiscordMember>();
                try
                {
                    members = (await me.Channel.Guild.GetAllMembersAsync()).ToList();
                    members.Sort(delegate(DiscordMember x, DiscordMember y)
                    {
                        int xdiff = x.JoinedAt.CompareTo(y.JoinedAt);
                        return xdiff;
                    });
                }
                catch
                {
                    // ignored
                }

                DiscordMember dmember =
                    await me.Channel.Guild.GetMemberAsync(Convert.ToUInt64(me.MentionedUser.DiscordUserId));

                string info = "";
                info += $" - Created at: {dmember.CreationTimestamp.DateTime}\n";
                info += $" - Joined at: {dmember.JoinedAt}\n";
                try
                {
                    info += $" - Game: {dmember.Presence.Activity.Name}\n";
                }
                catch
                {
                    info += " - Game: failed to get game info.\n";
                }

                info += $" - ID: {dmember.Id}\n";
                info +=
                    $" - Join pos: {members.IndexOf(dmember)}/{me.Channel.Guild.MemberCount} ({me.Channel.Guild.MemberCount - members.IndexOf(dmember)} after)\n";
                info += $" - Recorded message count: {me.MentionedDSUser.MessageCount}\n";
                info += $" - Warn count: {me.MentionedDSUser.Warnings.Count}\n";

                if (me.MentionedDSUser.Nicknames?.Count > 0)
                {
                    info += $" - Nickname history: ``` - {string.Join("\n - ", me.MentionedDSUser.Nicknames)}```\n";
                }

                // info += $" - Invites used: ``` - {string.Join("\n -", me.MentionedDSUser.InvitesUsed)}```\n";
                info += $" - Credits: {me.MentionedDSUser.GlobalUser.Credits}\n";

                /*if (me.MentionedDSUser.IsStaff)
                {
                    info += $" - Is staff\n";
                }*/

                if (me.MentionedDSUser.Muted)
                {
                    info +=
                        $" - Muted: {me.MentionedDSUser.Muted} (Until: {me.MentionedDSUser.UnmuteBy}, {(me.MentionedDSUser.UnmuteBy - DateTime.Now).TotalSeconds} seconds remaining)\n";
                }

                await me.Channel.SendMessageAsync($"User info for {dmember}:\n" + info);
                return null;
            }
        };
    }

    //serverinfo command, displays information about the current server
    public static Command ServerInfoCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "serverinfo",
            Category = "Core",
            Description = "Info about the server",
            action = async (me, ce) =>
            {
                try
                {
                    var serverinfo = @$"Server info for {me.Channel.Guild}:
                                 - Member count: {me.Channel.Guild.MemberCount} (DB: {me.Server.DServerUsers.Count})
                                 - Channel count: {me.Channel.Guild.Channels.Count}
                                 - Icon: <{me.Channel.Guild.IconUrl}>\n
                                 - Created at: {me.Channel.Guild.CreationTimestamp}
                                 - Banner: <https://cdn.discordapp.com/banners/{me.Channel.Guild.Id}/{me.Channel.Guild.Banner}.png?size=2048>
                                 - Splash: <{me.Channel.Guild.SplashUrl}>
                                 - AFK Channel: {me.Channel.Guild.AfkChannel} after {me.Channel.Guild.AfkTimeout} seconds
                                 - Default message notification settings: {me.Channel.Guild.DefaultMessageNotifications}
                                 - Description: {me.Channel.Guild.Description}
                                 - Embed enabled: {me.Channel.Guild.WidgetEnabled}
                                 - Emote count: {me.Channel.Guild.Emojis.Count}
                                 - Explicit content filter: {me.Channel.Guild.ExplicitContentFilter}
                                 - Features: {string.Join(", ", me.Channel.Guild.Features)}
                                 - Large: {me.Channel.Guild.IsLarge}
                                 - MFA level: {me.Channel.Guild.MfaLevel}
                                 - Owner: {me.Channel.Guild.Owner}
                                 - Boost count: {me.Channel.Guild.PremiumSubscriptionCount}
                                 - Boost tier: {me.Channel.Guild.PremiumTier}
                                 - Role count: {me.Channel.Guild.Roles.Count}
                                 - System channel: {me.Channel.Guild.SystemChannel}
                                 - Vanity URL code: {me.Channel.Guild.VanityUrlCode}
                                 - Verification level: {me.Channel.Guild.VerificationLevel}
                                 - Lockdown: {me.Server.LockdownEnabled}
                                 - Message count: {me.Server.MessageCount}
                                 - Reset on leave: {me.Server.ResetOnLeave}
                                 - Autorole on join: {me.Server.AutoroleEnabled}
                                 - Moderation enabled: {me.Server.ModerationEnabled}"
                        .Replace("                                 ", " ");
                    await me.Channel.SendMessageAsync(serverinfo);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    throw;
                }

                return null;
            }
        };
    }

    //donate command, shows info about how to donate, and notable donators
    public static Command DonateCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "donate",
            Category = "Core",
            Description = "Info about donating, list of donators",
            action =
                async (me, ce) =>
                {
                    await me.Channel.SendMessageAsync(
                        @"You can donate at <https://paypal.me/TheArcaneBrony> or <https://patreon.com/TheArcaneBrony>.
Top donators:

Kinoshita Shimizu: >500 EUR (hardware)
CHRONOBODI: 55 EUR (PSU)"
                    );
                    return null;
                }
        };
    }

    //premium command: gives a user info about <botcore> premium
    public static Command PremiumCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "premium",
            Category = "Core",
            Description = "Info about premium membership",
            action = async (me, ce) =>
            {
                await me.Channel.SendMessageAsync(
                    @"You can donate at <https://paypal.me/TheArcaneBrony> or <https://patreon.com/TheArcaneBrony>.

Premium will offer you more features and credits!
Pricing: 1$/month
You can pay a multiple of this price to add more time!
Make sure to include your *__Discord user ID__* in the description so I can activate your features!"
                );
                return null;
            }
        };
    }

    //botinfo command, posts bot statistics
    //NOTE: requires the member info intent for member counts to work
    public static Command BotInfoCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "botinfo",
            Category = "Core",
            Description = "Information about the bot",
            action = async (me, ce) =>
            {
                try
                {
                    //get git state
                    var gs = VersionUtils.GetGitStatus();
                    string state = "In sync";
                    //check if behind, ahead or modified
                    if (gs != (0, 0, 0, 0))
                    {
                        //build state message, only including relevant stuff
                        List<string> tags = new();
                        if (gs.Behind > 0) tags.Add($"{gs.Behind} commits behind");
                        if (gs.Ahead > 0) tags.Add($"{gs.Ahead} commits ahead");
                        if (gs.Modified + gs.Staged > 0) tags.Add($"{gs.Modified + gs.Staged} files modified locally");
                        if (gs.Staged > 0) tags.Add($"whereof {gs.Staged} staged");
                        state = string.Join(", ", tags);
                    }

                    //get db latency
                    var dblat = Stopwatch.StartNew();
                    me.Db.Database.CanConnect();
                    dblat.Stop();

                    //send message
                    await me.Channel.SendMessageAsync(
                        $"Servers: {me.Bot.DiscordClient.Guilds.Count} ({me.Bot.Bot.ServerCount} when last checked)\n" +
                        $"Total channels: {me.Bot.DiscordClient.Guilds.Sum(x => x.Value.Channels.Count)}\n" +
                        $"Total users: {me.Bot.DiscordClient.Guilds.Sum(x => x.Value.MemberCount)} ({bot.Db.DServerUsers.Count(x => x.DServer == me.Server)} of which in db)\n" +
                        //$"Users in database: {bot.Db.GlobalUsers.Count()}\n" +
                        $"Commands: {bot.CommandManager.Commands.Count}\n" +
                        $"Uptime: {Process.GetCurrentProcess().StartTime.Subtract(DateTime.Now).Duration()} (since <t:{new DateTimeOffset(Process.GetCurrentProcess().StartTime.ToUniversalTime()).ToUnixTimeSeconds()}>)\n" +
                        $"Latency: {me.Bot.DiscordClient.Ping} ms to Discord, {dblat.ElapsedMilliseconds} ms to database\n" +
                        $"Version: {VersionUtils.GetLongVersion()} ({state})");
                }
                catch (Exception ee)
                {
                    //TODO: move error handling to command handling
                    await me.Channel.SendMessageAsync(
                        $"Something has gone wrong processing this command: ```\n{ee}```");
                }

                return null;
            }
        };
    }
}