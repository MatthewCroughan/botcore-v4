﻿using System.Net;
using BotCore.DbExtras;
using BotCore.Util;
using DSharpPlus.Entities;
using Newtonsoft.Json;

namespace BotCore.Commands.Commands.Groups;

internal class FunCommands : CommandGroup
{
    private static readonly Random rnd = new();

    //avatar command, returns the avatar of the user that runs this command
    public static Command AvatarCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "avatar",
            Category = "Fun",
            Description = "Returns the avatar of the mentioned user",
            action =
                async (me, ce) =>
                {
                    // user mentioned
                    if (me.Message.MentionedUsers.Count >= 1)
                        await me.Channel.SendMessageAsync(me.Message.MentionedUsers[0].AvatarUrl.Split('?')[0] +
                                                          "?size=2048");
                    // no users mentioned
                    else
                        await me.Channel.SendMessageAsync(me.Message.Author.AvatarUrl.Split('?')[0] + "?size=2048");

                    return null;
                }
        };
    }

    // Senko Noises Command, decided to reimplement this because why not?
    public static Command SenkoNoisesCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "senkonoises",
            Category = "Fun",
            Description = "Yes... It's noises... That's about it",
            action =
                async (me, ce) =>
                {
                    await me.Channel.SendMessageAsync("https://www.youtube.com/watch?v=UZhKFxKmoIQ");
                    return null;
                }
        };
    }

    public static Command FloofCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "floof",
            Category = "Fun",
            Description = "Floof Pics... Yes",
            action = async (me, ce) =>
            {
                var fs = File.OpenRead(ImageRepo.GetRandom("floof"));
                await me.Channel.SendMessageAsync(builder => builder.WithFile(fs));
                fs.Close();

                return null;
            }
        };
    }

    // Headpat Command, sends a random file from the headpats folder if it exists, otherwise sends an error
    public static Command HeadpatCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "headpat",
            Category = "Fun",
            Description = "Headpats for everyone!",
            action = async (me, ce) =>
            {
                var fs = File.OpenRead(ImageRepo.GetRandom("headpats"));
                await me.Channel.SendMessageAsync(builder => builder.WithFile(fs));
                fs.Close();

                return null;
            }
        };
    }

    // Spray Command, uploads spray.jpg if it exists, otherwise sends an error message.
    public static Command SprayCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "spray",
            Category = "Fun",
            Description = "BAD! UwU",
            action = async (me, ce) =>
            {
                var fs = File.OpenRead(ImageRepo.GetImage("spray.jpg"));
                
                foreach (var usr in me.Message.MentionedUsers)
                {
                    var i = ce.Args.IndexOf(usr.Mention);
                    ce.Args[i] = $"*{usr.Username}*";
                }
                foreach (var usr in me.Message.MentionedRoles)
                {
                    var i = ce.Args.IndexOf(usr.Mention);
                    ce.Args[i] = $"*{usr.Name}*";
                }

                await me.Channel.SendMessageAsync(builder =>
                    builder.WithFile(fs).WithContent($"{string.Join(" ", ce.Args)}\n\n     - {me.Message.Author.Username}"));
                fs.Close();

                return null;
            }
        };
    }

    //xkcd command
    public static Command XkcdCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "xkcd",
            Description = "Display an xkcd comic by id",
            Category = "Fun",
            action = async (me, ce) =>
            {
                //get data
                dynamic resp =
                    JsonConvert.DeserializeObject(
                        new WebClient().DownloadString($"https://xkcd.com/{ce.Args[0]}/info.0.json"));
                //create embed
                var de = new DiscordEmbedBuilder();
                de.Title = resp.title;
                de.ImageUrl = resp.img;
                de.Footer = new DiscordEmbedBuilder.EmbedFooter
                {
                    Text = resp.alt
                };
                //send
                await me.Channel.SendMessageAsync(new DiscordMessageBuilder().AddEmbed(de.Build()));
                return null;
            }
        };
    }
}