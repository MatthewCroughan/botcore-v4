using System.Diagnostics;
using ArcaneLibs.Logging;
using BotCore.Commands;
using BotCore.DataModel;
using BotCore.Handlers.Message;
using BotCore.Handlers.Reaction;
using BotCore.Util.Systemd;
using DSharpPlus;
using DSharpPlus.EventArgs;
using DSharpPlus.VoiceNext;

namespace BotCore.DbExtras;

public class BotImplementation
{
    //loggers
    public readonly LogManager AlertLog = new();
    public readonly LogManager SysLog = new();
    public readonly LogManager EventLog = new();
    
    public readonly Bot Bot;
    public readonly CommandManager CommandManager;
    public readonly Db Db;
    public readonly DiscordClient DiscordClient;
    public readonly MessageHandler MessageHandler;
    public readonly QuoteHandler QuoteHandler;

    public BotImplementation(Db db,Bot bot)
    {
        //start tracking time
        var transaction = Util.Sentry.StartTransaction("botcore_init");
        Console.WriteLine($"Initialising as {bot.FriendlyName}...");
        //store references
        Bot = bot;
        bot.Db = db;
        Db = db;
        //initialise new handlers
        CommandManager = new CommandManager(this);
        MessageHandler = new MessageHandler(this);
        QuoteHandler = new QuoteHandler(this);
        //create client
        DiscordClient = CreateClient(db, bot);
        //bind client events
        BindEvents(DiscordClient);
        //load base commands
        Console.WriteLine($"Loading commands for {bot.FriendlyName}...");
        CommandManager.Init();
        transaction.Finish();
        //log in
        Console.WriteLine($"Connecting as {Bot.FriendlyName}...");
        transaction = Util.Sentry.StartTransaction("botcore_connect");
        DiscordClient.ConnectAsync().GetAwaiter().GetResult();
        transaction.Finish();
    }

    private void BindEvents(DiscordClient discordClient)
    {
        DiscordClient.MessageCreated += MessageHandler.HandleMessage;
        DiscordClient.ThreadCreated += OnThreadCreated;
        DiscordClient.ClientErrored += OnClientError;
        DiscordClient.Ready += OnConnected;
        DiscordClient.MessageReactionAdded += QuoteHandler.ReactionAdded;
        DiscordClient.MessageReactionRemoved += QuoteHandler.ReactionRemoved;
        DiscordClient.Zombied += OnClientZombied;
    }

    private DiscordClient CreateClient(Db db, Bot bot)
    {
        var discordClient = new DiscordClient(new DiscordConfiguration
        {
            Intents = DiscordIntents.All,
            Token = bot.Token,
            AutoReconnect = true
        });
        discordClient.UseVoiceNext(new()
        {
            AudioFormat = AudioFormat.Default,
            PacketQueueSize = 10
        });
        return discordClient;
    }

    //events
    private async Task OnClientError(DiscordClient _, ClientErrorEventArgs eventArgs)
    {
        Console.WriteLine($"An exception occured in {eventArgs.EventName}:\n```cs\n{eventArgs.Exception}\n```");
    }

    private Task OnClientZombied(DiscordClient sender, ZombiedEventArgs e)
    {
        EventLog.Log($"Client zombie'd! Failed to heartbeat {e.Failures} times! Restarting...");
        Console.WriteLine($"Client zombie'd! Failed to heartbeat {e.Failures} times! Restarting...");
        Thread.Sleep(2000);
        Environment.Exit(0);

        return Task.CompletedTask;
    }
    private async Task OnThreadCreated(DiscordClient _, ThreadCreateEventArgs eventArgs)
    {
        await eventArgs.Thread.JoinThreadAsync();
    }

    private Task OnConnected(DiscordClient sender, ReadyEventArgs args)
    {
        Systemd.NotifyReady();
        if(!Debugger.IsAttached)
            EventLog.Log($"{Bot.FriendlyName} started up on {Environment.MachineName + (Debugger.IsAttached ? " (Debugging)" : "")}.");
        return Task.CompletedTask;
    }
}