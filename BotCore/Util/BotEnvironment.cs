using System.Runtime.InteropServices;

namespace BotCore.Util;

public static class BotEnvironment
{
    public static bool IsLinux => RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

    public static string SolutionDir
    {
        get
        {
            var cd = Environment.CurrentDirectory;
            while (!Directory.GetFiles(cd).Any(x => x.EndsWith(".sln")) && cd.Length < 250) cd += "/..";
            return cd;
        }
    }
    public static string ProjectDir
    {
        get
        {
            var cd = Environment.CurrentDirectory;
            while (!Directory.GetFiles(cd).Any(x => x.EndsWith(".csproj")) && cd.Length < 250) cd += "/..";
            return cd;
        }
    }
}