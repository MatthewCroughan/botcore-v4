using DSharpPlus.Entities;

namespace BotCore.Util.Extensions.DSharpPlus;

public static class DiscordChannelExtensions
{
    public static async Task<List<DiscordMessage>> GetAllMessagesAfterAsync(this DiscordChannel channel, ulong firstMessage = 0)
    {
        List<DiscordMessage> messages = new();
        ulong lastMessage = firstMessage;
        while (lastMessage < channel.LastMessageId)
        {
            IReadOnlyList<DiscordMessage> msgs = await channel.GetMessagesAfterAsync(lastMessage);
            messages.AddRange(msgs);
            lastMessage = msgs.Count > 0 ? msgs.Max(x => x.Id) : lastMessage;
            Console.WriteLine($"GetAllMessagesAsync: got {messages.Count} messages so far...");
        }

        return messages;
    }
}