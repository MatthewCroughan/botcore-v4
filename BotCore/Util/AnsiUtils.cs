namespace BotCore.Util;

public static class AnsiUtils
{
    public static string StripDiscordUnsupported(string text)
    {
        return text
                .Replace("[?25l", "")
                .Replace("[?7l", "")
                .Replace("[19A", "")
                .Replace("[20A", "")
                .Replace("[9999999D", "")
                .Replace("[41C", "")
                .Replace("[43C", "")
                .Replace("[?25h", "")
                .Replace("[?7h", "")
                .Replace("[m", "")
            ;
    }
}

public static class AnsiExtensions
{
    public static string StripDiscordUnsupported(this string text)
    {
        return AnsiUtils.StripDiscordUnsupported(text);
    }
}