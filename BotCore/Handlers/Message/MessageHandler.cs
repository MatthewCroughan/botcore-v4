using ArcaneLibs.Logging;
using ArcaneLibs.Logging.LogEndpoints;
using BotCore.Classes;
using BotCore.DbExtras;
using BotCore.Util;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Exceptions;

namespace BotCore.Handlers.Message;

public class MessageHandler
{
    public BotImplementation bot;

    public LogManager log = new()
    {
        LogTime = true
    };

    public MessageHandler(BotImplementation bot)
    {
        this.bot = bot;
        log.Prefix = $"{bot.Bot.FriendlyName}/";
        log.AddEndpoint(new ConsoleEndpoint());
    }

    public Task HandleMessage(DiscordClient sender, MessageCreateEventArgs msg)
    {
        var transaction = Util.Sentry.StartTransaction("handle_message");
        
        //auto publish gitlogs
        CrosspostGitlogs(msg.Message);
        
        //get data
        var environment = MessageEnvironmentBuilder.Build(bot, msg.Message);
        
        //handle xp and interactions
        Xp.CheckMessage(environment);
        MessageInteractions.CheckMessage(environment);
        CustomSlowmodeHandler.CheckMessage(environment);
        MessageScamHandler.CheckMessage(environment);
        
        //handle commands
        if (environment.Message.Content.StartsWith(environment.Server.Prefix))
        {
            var Span = transaction.StartChild("CommandHandling");
            bot.CommandManager.Execute(environment);
            Span.Finish();
        }

        log.Log($"Finished handling message: {environment.Message}");
        //update user and guild info
        UpdateInfo(environment);
        environment.Db.SaveChanges();
        
        transaction.Finish();
        return null;
    }

    private void UpdateInfo(MessageEnvironment env)
    {
        //guild
        if (env.Channel.Guild != null)
        {
            env.Server.Name = env.Channel.Guild.Name;
            env.Server.IconUrl = env.Channel.Guild.GetIconUrl(ImageFormat.Auto) ??
                             "https://cdn.discordapp.com/embed/avatars/0.webp?size=512";
            env.Server.MemberCount = env.Channel.Guild.MemberCount;
        }
        //member
        env.User.Username = env.Message.Author.Username;
        env.User.Discriminator = env.Message.Author.Discriminator;
        env.User.AvatarUrl = env.Message.Author.GetAvatarUrl(ImageFormat.Auto) ??
                          "https://cdn.discordapp.com/embed/avatars/0.webp?size=512";
        env.User.IsBot = env.Message.Author.IsBot;
        env.User.IsPremium = DateTime.Now <= env.User.PremiumSince + env.User.PremiumDuration;
    }

    private void CrosspostGitlogs(DiscordMessage msg)
    {
        if (msg.ChannelId == 806142683265368125)
            try
            {
                msg.Channel.CrosspostMessageAsync(msg);
            }
            catch (ArgumentException)
            {
                //ignore
            }
            catch (UnauthorizedException e)
            {
                bot.AlertLog.Log("Bot doesn't have permission to crosspost:");
                bot.AlertLog.Log(e.ToString());
            }
            catch (Exception e)
            {
                bot.AlertLog.Log(e.ToString());
            }
    }
}