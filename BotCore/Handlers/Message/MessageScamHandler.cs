﻿using System.Diagnostics;
using BotCore.Classes;
using BotCore.DbExtras;
using DSharpPlus.Entities;

namespace BotCore.Handlers.Message;

public class MessageScamHandler
{
    private static string[] ScamDomains = new string[0];
    private static DateTime LastUpdated = DateTime.UnixEpoch;

    public static async void CheckMessage(MessageEnvironment me)
    {
        if (me.Message.Author.IsBot) return;
        if (!Debugger.IsAttached) return;
        if (LastUpdated <= DateTime.Now.Subtract(TimeSpan.FromMinutes(15)))
        {
            ScamDomains = GetScamDomains(me.Bot);
            LastUpdated = DateTime.Now;
        }
        //return; //dont use yet
        
        var doms = GetUrls(me.Message.Content);
        if (doms.Any())
        {
            foreach (var dom in doms)
            {
                SendMessage(me, dom);
            }

            me.Db.AntiScamLogs.Add(new()
            {
                Server = me.Server,
                User = me.DSUser,
                MessageText = me.Message.Content
            });
            HandlePunishment(me);
        }
    }

    public static void HandlePunishment(MessageEnvironment me)
    {
        if (me.Channel.Guild.GetMemberAsync(me.Bot.DiscordClient.CurrentUser.Id).Result.Hierarchy <
            ((DiscordMember) me.Message.Author).Hierarchy)
        {
            
        }
    }

    private static string[] GetUrls(string msg)
    {
        return msg.Replace("*","").Replace("`","").Replace("|","")
            .Replace('\n', ' ').Split(' ').Where(x => x.StartsWith("http"))
            .Select(x => x.Split('/')[2]).Where(ScamDomains.Contains).ToArray();
    }

    private static async void SendMessage(MessageEnvironment me, string domain)
    {
        me.Channel.SendMessageAsync($"errrr, you said: {domain}");
    }

    private static string[] GetScamDomains(BotImplementation bot)
    {
        HttpClient hc = new();
        var ver = bot.Bot.CoreVer.Split('+');
        hc.DefaultRequestHeaders.Add("User-Agent",
            $"BotCore.{bot.Bot.FriendlyName}/{ver[0]} ({ver[1]}; The Arcane Brony#9669)");
        Console.WriteLine("Fetching scam domains!");
        return hc.GetStringAsync("https://phish.sinking.yachts/v2/text").Result.Split('\n');
    }
}