using ArcaneLibs.Logging.LogEndpoints;
using DSharpPlus.Entities;

namespace BotCore.Classes;

public class DiscordEndpoint : BaseEndpoint
{
    private DiscordChannel _channel;
    public DiscordEndpoint(DiscordChannel channel)
    {
        _channel = channel;
    }

    public override void Write(string text)
    {
        _channel.SendMessageAsync("```cs\n" + text + "```");
    }
}