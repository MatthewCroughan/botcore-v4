using BotCore.Commands;
using BotCore.DbExtras;
using Sentry;

namespace BotCore.Classes;

public struct CommandEnvironment
{
    public Command Command;
    public CommandStatus CommandStatus;
    public List<string> Args { get; set; }
    public BotImplementation BotImplementation { get; set; }
    public ISpan Span { get; set; }
}