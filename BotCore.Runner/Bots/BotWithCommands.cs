using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;
using Monitor = BotCore.Monitoring.Monitor;

namespace BotCore.Runner.Bots;

public class BotWithCommands
{
    public static BotImplementation Run(string name, params Type[] groups)
    {
        var db = Db.GetPostgres();
        var bot = db.GetBot(name);
        var bi = new BotImplementation(db, bot);
        foreach (var cmdgroup in groups)
        {
            bi.CommandManager.RegisterCommandsInType(cmdgroup);
        }
        Monitor.StartMonitoring(bi);
        return bi;
    }
}