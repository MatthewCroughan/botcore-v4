using BotCore.Commands;
using BotCore.Commands.Commands;
using BotCore.DbExtras;

namespace Bots.FosscordBot.Commands;

public class Github : CommandGroup
{
    //g, return github pull request or issue for repo
    public static Command PRIssueCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "g",
            Category = "Github",
            Description = "return github pull request/issue link for repo",
            action = async (me, ce) =>
            {
                await me.Channel.SendMessageAsync(
                    $"https://github.com/fosscord/{(ce.Args[0] == "fosscord" ? ce.Args[0] : "fosscord-" + ce.Args[0])}/issues/{ce.Args[1]}");
                return null;
            }
        };
    }

    //c, return commit for repo
    public static Command CommitCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "c",
            Category = "Github",
            Description = "return github commit link for repo",
            action = async (me, ce) =>
            {
                await me.Channel.SendMessageAsync(
                    $"https://github.com/fosscord/{(ce.Args[0] == "fosscord" ? ce.Args[0] : "fosscord-" + ce.Args[0])}/commit/{ce.Args[1]}");
                return null;
            }
        };
    }
}