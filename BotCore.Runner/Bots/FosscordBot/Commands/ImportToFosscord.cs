using BotCore.Commands;
using BotCore.Commands.Commands;
using BotCore.DataModel;
using BotCore.DbExtras;
using Npgsql;

namespace Bots.FosscordBot.Commands;

public class ImportToFosscord : CommandGroup
{
    //itf, import to fosscord
    public static Command ImportToFosscordCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "itf",
            Category = "owner",
            Description = "copy guild+content to fosscord instance",
            action = async (me, ce) =>
            {
                var cfg = DbConfig.Read();
                cfg.Save();
                NpgsqlConnection conn = new NpgsqlConnection(
                    $"Host=thearcanebrony.net;Database=fosscord;Username={cfg.Username};Password={cfg.Password};Port={cfg.Port};Include Error Detail=true");
                // $"Host={cfg.Host};Database=fosscord;Username={cfg.Username};Password={cfg.Password};Port={cfg.Port};Include Error Detail=true");
                conn.Open();
                if (new NpgsqlCommand($"select * from guilds where id = '{me.Channel.Guild.Id}'", conn).ExecuteScalar() == null)
                {
                    Console.WriteLine("guild doesnt exist");
                    var cmdstr =
                        $"INSERT INTO guilds (id,afk_timeout,default_message_notifications,explicit_content_filter,max_members,max_presences,max_video_channel_users,member_count,presence_count,mfa_level,\"name\",owner_id,preferred_locale,premium_subscription_count,premium_tier,region,system_channel_flags,unavailable,verification_level,welcome_screen,widget_enabled,nsfw_level,nsfw,features)\n\tVALUES ('{me.Channel.Guild.Id}',300,0,0,250000,250000,200,1,0,0,'{me.Channel.Guild.Name}','900136759973961752','en-US',600,3,'fosscord',0,false,0,'{{\"enabled\":false,\"description\":\"No description\",\"welcome_channels\":[]}}',false,0,false,'');";
                    Console.WriteLine(new NpgsqlCommand(cmdstr, conn).ExecuteNonQuery());
                    int maxindex = (int) new NpgsqlCommand($"select max(index) from members", conn).ExecuteScalar();
                    new NpgsqlCommand($"insert into members (index, id, guild_id, joined_at, deaf, mute, pending, settings) values ({maxindex}, '900136759973961752', '{me.Channel.Guild.Id}', '2022-04-21 06:51:18.776', false, false, false, '{{\"channel_overrides\":[],\"message_notifications\":0,\"mobile_push\":true,\"muted\":false,\"suppress_everyone\":false,\"suppress_roles\":false,\"version\":0}}');", conn).ExecuteNonQuery();
                }
                else
                {
                    Console.WriteLine("guild exists");
                }


                await conn.CloseAsync();
                return null;
            },
            CanRun = (me, ce) => Task.FromResult(me.Message.Author.Id == 84022289024159744)
        };
    }
}