using BotCore.DataModel;
using BotCore.Web.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace BotCore.Web.Controllers.API;

[Controller]
[Route("/")]
public class DiscordAuthController : Controller
{
    private readonly Db _db;

    public DiscordAuthController(Db db)
    {
        _db = db;
    }

    [HttpGet("discord_oauth")]
    public object DiscordOauth(string code = "")
    {
        if (code.Length < 3) return Redirect($"https://discord.com/api/oauth2/authorize?client_id=980346764613804032&redirect_uri={(Request.IsHttps?"https":"http")}%3A%2F%2F{Request.Host.Host}%2Fdiscord_oauth&response_type=code&scope=identify");
        Console.WriteLine(Request.Host);
        Console.WriteLine("Got code: " + code);
        //Console.WriteLine(JsonConvert.SerializeObject(resp, Formatting.Indented));
        var bot = Resolvers.GetBotByHost(_db, Request.Host.Host);
        DoOauth(code, $"{(Request.IsHttps?"https":"http")}://{Request.Host.Host}/discord_oauth");
        
        return Resolvers.ReturnFileWithVars("Resources/Pages/index.html", _db, bot);
    }

    private void DoOauth(string code, string redirectUrl)
    {
        var clientId = _db.Config.GetString("web.oauth.client_id", "not set");
        var clientSecret = _db.Config.GetString("web.oauth.client_secret", "not set");
        
        var hc = new HttpClient();
        var resp = hc.PostAsync("https://discord.com/api/v10/oauth2/token", new FormUrlEncodedContent(new Dictionary<string, string>()
        {
            ["client_id"] = clientId,
            ["client_secret"] = clientSecret,
            ["grant_type"] = "authorization_code",
            ["code"] = code,
            ["redirect_uri"] = redirectUrl
        })).Result;
        
        Console.WriteLine(resp.Content.ReadAsStringAsync().Result);
    }
}