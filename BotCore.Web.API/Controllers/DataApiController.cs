using BotCore.DataModel;
using BotCore.Web.Frontend;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BotCore.Web.API.Controllers;

[Controller]
[Route("/api")]
public class DataApiController : Controller
{
    private readonly Db _db;

    public DataApiController(Db db)
    {
        _db = db;
    }

    [HttpGet("thisbot")]
    public object GetThisBot()
    {
        return Logic.GetBotByDomain(Request.Host.Host);
    }

    [HttpGet("bot")]
    public object GetBot(string? name = null, int? id = null)
    {
        if (name == null && id == null) return NotFound("No parameter specified! Expected name/id.");
        if (name != null && _db.Bots.Any(x => x.Name == name)) return _db.Bots.First(x => x.Name == name);
        if (id != null && _db.Bots.Any(x => x.BotId == id)) return _db.Bots.First(x => x.BotId == id);
        return NotFound("No results found!");
    }

    [HttpGet("server")]
    public object GetServer(int? bid = null, ulong? sid = null, ulong? id = null)
    {
        if (sid == null && id == null && bid == null)
            return NotFound("No parameter specified! Expected bid, and sid/id.");
        if (id != null && _db.Servers.Any(x => x.DiscordServerId == id.ToString() && x.BotId == bid))
            return _db.Servers.First(x => x.DiscordServerId == id.ToString() && x.BotId == bid);
        if (sid != null && _db.Servers.Any(x => x.ServerId == (long) sid && x.BotId == bid))
            return _db.Servers.First(x => x.ServerId == (long) sid && x.BotId == bid);
        return NotFound("No results found!");
    }

    [HttpGet("servers")]
    public object GetServers(int? bid = null)
    {
        if (bid == null) return NotFound("No parameter specified! Expected bid.");
        if (_db.Servers.Any(x => x.BotId == bid))
            return _db.Servers.Where(x => x.BotId == bid).Select(x => new
                {x.ServerId, x.DiscordServerId, x.MemberCount, x.OnlineMemberCount, x.IconUrl, x.Name}).AsNoTracking();
        return NotFound("No results found!");
    }


    [HttpGet("globaluser")]
    public object GetGlobalUser(int? uid = null, ulong? id = null)
    {
        if (uid == null && id == null) return NotFound("No parameter specified! Expected uid or id.");
        if (uid != null && _db.GlobalUsers.Any(x => x.GlobalUserId == uid))
            return _db.GlobalUsers.First(x => x.GlobalUserId == uid);
        if (id != null && _db.GlobalUsers.Any(x => x.DiscordUserId == id.ToString()))
            return _db.GlobalUsers.First(x => x.DiscordUserId == id.ToString());
        return NotFound("No results found!");
    }

    [HttpGet("user")]
    public object GetUser(int? uid = null, int? bid = null, ulong? sid = null, ulong? dsid = null, ulong? duid = null,
        int? guid = null)
    {
        if (uid == null && (sid == null && dsid == null || bid == null && (duid == null || guid == null)))
            return NotFound("No parameter specified! Expected uid, or bid, sid/dsid and duid/guid.");
        if (uid != null) return _db.DServerUsers.First(x => x.UserId == uid);
        if (bid == null || sid == null && dsid == null) return NotFound("No results found!");

        var bot = GetBot(id: bid);
        if (bot.GetType() != typeof(Bot)) return NotFound("No bot found!");
        var srv = GetServer(bid, sid, dsid);
        if (srv.GetType() != typeof(Server)) return NotFound("No such server found!");
        var gu = GetGlobalUser(guid, duid);
        if (gu.GetType() != typeof(GlobalUser)) return NotFound("No global user found!");
        if (_db.DServerUsers.Any(x => x.GlobalUser == gu && x.DServer == srv))
            return _db.DServerUsers.First(x => x.GlobalUser == gu && x.DServer == srv);
        return NotFound("No guid/duid set??");
    }

    [HttpGet("quotes")]
    public object GetQuotes(int? uid = null)
    {
        if (uid == null) return NotFound("Expected user id (see /api/user)");
        var usr = GetUser(uid);
        if (usr.GetType() != typeof(DServerUser)) return NotFound("No user found with this id!");
        if (_db.Quotes.Any(x => x.Author == usr)) return _db.Quotes.Where(x => x.Author == usr);
        return NotFound("No results found!");
    }

    [HttpGet("users")]
    public object GetUsers(int? bid = null, ulong? sid = null, ulong? dsid = null)
    {
        if (sid == null && bid == null) return NotFound("No parameter specified! Expected bid and sid.");
        var bot = GetBot(id: bid);
        if (bot.GetType() != typeof(Bot)) return NotFound("Couldn't find bot!");
        var srv = GetServer(bid, sid, dsid);
        if (srv.GetType() != typeof(Server)) return NotFound("Couldn't find server!");
        if (_db.DServerUsers.Any(x => x.DServer == srv))
            return _db.DServerUsers.Where(x => x.DServer == srv).Include(x => x.GlobalUser).Select(x =>
                new
                {
                    x.UserId,
                    x.GlobalUser.DiscordUserId,
                    x.GlobalUser.AvatarUrl,
                    x.GlobalUser.Username,
                    x.GlobalUser.Discriminator,
                    x.XpLevel,
                    x.Xp,
                    x.XpGoal,
                    x.XpProgress
                }).AsNoTracking();
        return NotFound("No results found!");
    }

    [HttpGet("avatar")]
    public object UpdateAvatar(ulong? id = null, bool invalid = false)
    {
        // return NotFound("");
        if (id == null) return NotFound("No parameter specified! Expected id.");
        if (invalid)
        {
            //Console.WriteLine("Adding invalid avatar to queue: " + id);
            //DataStore.AvatarsToUpdate.Add(id.Value);
        }

        return Redirect("https://cdn.discordapp.com/embed/avatars/0.png");
    }
}