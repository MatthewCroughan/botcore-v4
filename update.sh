#!/bin/sh
if [ "$EUID" -ne 0 ]
  then echo "Please run as root, also make sure root can read/write these files and folders!"
  exit
fi
if [ ! -f "DiscordBots.sln" ]; then
    echo "You must run this script from the project directory!"
    exit
fi

echo 'Pulling changes...'
git pull
git submodule init
git submodule update

echo 'Reinstalling...'
chmod +x install.sh
chmod +x update.sh
./install.sh